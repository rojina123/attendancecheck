<?php

use App\Helpers\General\HtmlHelper;

if(!function_exists('working_hours')){
    function working_hours($start, $end){
        if(is_null($end) || is_null($start)){
            return 0.0;
        }
        $e=strtotime($end);
        $s=strtotime($start);
        if($e<$s){
            $result=$e+(24*60*60-$s);
        }else{
            $result=($e - $s) ;
        }
  return $result;
    }
}
