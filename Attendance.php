<?php

namespace App\Models;


class Attendance extends Model
{	
protected  $fillable=['user_id’, ‘working_hour’];
    protected $dates=['date','punch_in_time','punch_out_time'];

//relationship with user   
 public function user () {
        return $this->belongsTo(User::class);
    }
}

// Calculate Total working hours
public function getWorkingHours()
{
return working_hours($this->attributes['punch_in_time'],$this->attributes['punch_out_time']);
}
