<?php

use Illuminate\Support\Facades\Schema;

class Attendances Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema:: create ('attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned ();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->date('date')->nullable ();
            $table->dateTime('punch_in_time')->nullable ();
            $table->dateTime('punch_out_time')->nullable ();
            $table->dateTime(‘working_hour’)->nullable ();
            $table->boolean('status')->default (1);
            $table->timestamps ();
        });


     public function down()
     {

     }   
}
