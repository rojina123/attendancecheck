<?php

namespace App\Htpp\Controllers\Backend;

use Illuminate\Http\Request;

// To add the attendance of the employee

public function Attendance (Request $request){
        $att=Attendance::Create([
            'user_id'=>$request->get('user_id'),
            'date'=>$request->get('date'),
            'punch_in_time'=>Carbon::parse($request->get('date').'	'.$request->get('start_time')),
            'punch_out_time'=>Carbon::parse($request->get('date').' '.$request->get('end_time')),
            'status'=>1,
) ]);
        if($att){
            return ['status'=>1,'message'=>'Successfully Added'];
        }else{
            return ['status'=>0,'message'=>'Couldnt create record'];
        }

     }



// To get least working employee info

public function getEmployee()
{
    $emp=Attendance::where('status',1)->min('working_hour')->get();
    return $emp;


}



